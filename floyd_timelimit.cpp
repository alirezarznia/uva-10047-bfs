//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/

#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>



#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<pii , long long >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-15
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);


typedef long long ll;
typedef long double ld;

using namespace std;


inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
		return 53 +(ch - '0');
	}
}
 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
/* int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; */ // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}

}
ll n ,m ,xs, ys, xe, ye;
string s[30];
vector< vector<ll> > vec;
ll GG(ll x, ll y, ll d, ll c){
    return (x*m+y+(d*5*n*m)+(c*n*m));
}

bool isok(ll x, ll y){
    return (x>=0 && x<n && y<m && y>=0 && s[x][y] != '#');
}
int main(){
  //  Test;
  //  booste;
    ll c= 0;
    while(cin>>n>>m){
        if(!n && !m ) break;
        if(c)
         cout<<endl;
        Rep(i, n) {
                        cin >>s[i];
                                Rep(j , m)  {
                                    if(s[i][j]=='S') xs= i , ys = j;
                                    if(s[i][j]=='T') xe =i , ye=j;
                                }
                }
        vec.assign(17275,vector<ll>(17275, inf));
        Rep(i ,n){
            Rep(j ,m){
                if(s[i][j] !='#'){
                    Rep(p ,5){
                        Rep(k ,4){
                             vec[GG(i ,j, k ,p)][GG(i , j, k, p)]=0;
                            vec[GG(i ,j, k ,p)][GG(i , j, (k+1)%4, p)]=1;
                            vec[GG(i ,j, k ,p)][GG(i , j, (k+3)%4 , p)]=1;
                            ll xx = i+dx[k] , yy =j+dy[k];
                            if(isok(xx, yy)){
                                vec[GG(i ,j, k ,p)][GG(xx , yy, k, (p+1)%5)]=1;
                                vec[GG(i ,j, k ,p)][GG(xx , yy, k , (p+1)%5)]=1;

                            }
                        }
                    }
                }
            }
        }
        Rep(k, 700){
            Rep(j ,700){
                Rep(i ,700) if(vec[i][k] + vec[k][j] < vec[i][j]) vec[i][j]=vec[i][k] + vec[k][j];
            }
        }
        ll ans = vec[GG(xs, ys , 2 , 0)][GG(xe, ye , 2 , 0)];
        ans = min(ans, vec[GG(xs, ys , 2 , 0)][GG(xe, ye, 1 , 0)]);
        ans = min(ans, vec[GG(xs, ys , 2 , 0)][GG(xe, ye , 0 , 0)]);
        ans = min(ans, vec[GG(xs, ys , 2 , 0)][GG(xe, ye , 3 , 0)]);
        printf("Case #%lld\n" , ++c);
        if(ans==inf) cout<<"destination not reachable"<<endl;
        else printf("minimum time = %lld sec\n" , ans);
     }
     return 0;
}
