//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/

#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>



#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<pii , long long >
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-15
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);


typedef long long ll;
typedef long double ld;

using namespace std;


inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
		return 53 +(ch - '0');
	}
}
 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
/* int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; */ // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}

}


ll arr[4][5][30][30];
ll n ,m ;
ll xs , ys;
string s[30];

struct Node{
    ll d ,c, x, y;
    Node(ll d, ll c, ll x, ll y){
        this->d=d;
        this->c=c;
        this->x=x;
        this->y=y;
    }
};
inline bool operator == (const Node &a, const Node &b){
    return((a.d == b.d) && (a.c == b.c) && (a.x == b.x) && (a.y == b.y));
}
ll B(){
    Node sn(2 , 0 , xs, ys);
    queue < Node>q;
    q.push(sn);
    while(!q.empty()){
        Node cur = q.front(); q.pop();
        if(s[cur.x][cur.y]=='T' && !cur.c) return arr[cur.d][cur.c][cur.x][cur.y];
        ll cl =(cur.c+1)%5; ll xx = cur.x + dx[cur.d] , yy = cur.y+dy[cur.d];
        if(xx>=0 && xx<n && yy>=0 && yy<m && s[xx][yy] !='#' && !arr[cur.d][cl][xx][yy]){
            Node ez(cur.d , cl ,xx ,yy);
            if(ez==sn) continue;
            q.push(ez);
            arr[cur.d][cl][xx][yy] = arr[cur.d][cur.c][cur.x][cur.y]+1;
        }
        if(!arr[(cur.d+1)%4][cur.c][cur.x][cur.y]){
            Node ez((cur.d+1)%4 , cur.c , cur.x, cur.y);
                 if(ez==sn) continue;
            q.push(ez);
            arr[(cur.d+1)%4][cur.c][cur.x][cur.y] = arr[cur.d][cur.c][cur.x][cur.y]+1;
        }
         if(!arr[(cur.d+3)%4][cur.c][cur.x][cur.y]){
         Node ez((cur.d+3)%4, cur.c , cur.x, cur.y);
              if(ez==sn) continue;
            q.push(ez);
            arr[(cur.d+3)%4][cur.c][cur.x][cur.y] = arr[cur.d][cur.c][cur.x][cur.y]+1;
        }
    }
    return -1;
}
int main(){
//    Test;
  //  booste;
    ll c= 0;
    while(cin>>n>>m){
        Set(arr , 0);
        if(!n && !m ) break;
        if(c)
         cout<<endl;
        Rep(i, n) {cin >>s[i]; Rep(j , m)  if(s[i][j]=='S') xs= i , ys = j;}
        printf("Case #%lld\n" , ++c);
        ll ans= B();
        if(ans==-1) cout<<"destination not reachable"<<endl;
        else printf("minimum time = %lld sec\n" , ans);
     }
     return 0;
}